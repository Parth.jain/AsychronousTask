package com.example.android.asynchronoustask;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button asynButton;
    private TextView runText;
    private EditText secondEdit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        secondEdit=findViewById(R.id.edit_seconds);
        runText=findViewById(R.id.result_textView);
        asynButton=findViewById(R.id.btnAsynch);

        asynButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTaskRunner runner=new AsyncTaskRunner();
                String sleeptime=secondEdit.getText().toString();
                runner.execute(sleeptime);
            }
        });

    }

    private class AsyncTaskRunner extends AsyncTask<String,String,String>{
        private String res;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping");
            try{
                int time=Integer.parseInt(params[0])*1000;
                Thread.sleep(time);
                res="Slept for "+params[0]+" Seconds";
            } catch (InterruptedException e) {
                e.printStackTrace();
                res=e.getMessage();
            }
            catch (Exception e){
                e.printStackTrace();
                res=e.getMessage();
            }
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            runText.setText(result);
        }

        @Override
        protected void onPreExecute() {
            progressDialog=ProgressDialog.show(MainActivity.this,"Wait Process Running",
                    "Wait for "+secondEdit.getText().toString());
        }

        @Override
        protected void onProgressUpdate(String... values) {
            //super.onProgressUpdate(values);
            runText.setText(values[0]);
        }
    }
}
